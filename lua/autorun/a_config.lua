-- toucanlib, A library developed in GLua to provide methods on common projects on the game Garry's Mod.
-- Copyright (C) 2023  MrMarrant aka BIBI.

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

toucanlib = {}
TOUCAN_LIB_CONFIG = {}
-- Don't touch this :)
TOUCAN_LIB_CONFIG.RootFolder = "toucanlib/"
-- Get the actual language.
TOUCAN_LIB_CONFIG.LangServer = GetConVar("gmod_language"):GetString()

-- Net Variable.
TOUCAN_LIB_CONFIG.SynchronizeDataClient = "TOUCAN_LIB_CONFIG.SynchronizeDataClient"
TOUCAN_LIB_CONFIG.SetDataClientToServer = "TOUCAN_LIB_CONFIG.SetDataClientToServer"
TOUCAN_LIB_CONFIG.SoundToPlayClientSide = "TOUCAN_LIB_CONFIG.SoundToPlayClientSide"

cvars.AddChangeCallback("gmod_language", function(name, old, new)
    TOUCAN_LIB_CONFIG.LangServer = new
end)

if (SERVER) then
    util.AddNetworkString( TOUCAN_LIB_CONFIG.SynchronizeDataClient )
    util.AddNetworkString( TOUCAN_LIB_CONFIG.SetDataClientToServer )
    util.AddNetworkString( TOUCAN_LIB_CONFIG.SoundToPlayClientSide )
end
-- [[ MANAGE FILES TO LOAD ]] 
--! Don't Touch this.
AddCSLuaFile(TOUCAN_LIB_CONFIG.RootFolder.."functions/general.lua")
include(TOUCAN_LIB_CONFIG.RootFolder.."functions/general.lua")