-- toucanlib, A library developed in GLua to provide methods on common projects on the game Garry's Mod.
-- Copyright (C) 2023  MrMarrant aka BIBI.

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- [[ SHARED ]]
/*
* Print Console message with somes decorations.
* @string mod The addon using the function.
* @string msg The message to print.
*/
function toucanlib.print(mod, msg)
	MsgC(
		Color(236, 240, 241), "[",
		Color(0, 100, 30), mod,
		Color(236, 240, 241), "] ",
		Color(236, 240, 241), msg
	) Msg("\n")
end


/*
* Allows to load all the language files that the addon can handle.
* @string path Path containing the language files.
* @string default Default language.
* @table handledLanguage Array containing the supported languages.
* @table langData Table containing all translations.
*/
function toucanlib.LoadLanguage(path, handledLanguage, langData )
    for key, value in ipairs(handledLanguage) do
        local filename = path .. value .. ".lua"
        include( filename )
        if SERVER then AddCSLuaFile( filename ) end
        assert(langData[value], "Language not found : ".. filename )
    end
end

/*
* Returns the element to be translated according to the server language.
* @table langData Array containing all translations.
* @string name Element to translate.
*/
function toucanlib.TranslateLanguage(langData, name)
    local langUsed = TOUCAN_LIB_CONFIG.LangServer
    if not langData[langUsed] then
        langUsed = "en" -- Default lang is EN.
    end
    return string.format( langData[langUsed][ name ] or "Not Found" )
end

/*
* Allows you to change all the files in a folder.
* @string path of the folder to load.
*/
function toucanlib.LoadDirectory(pathFolder)
    local files, directories = file.Find(pathFolder.."*", "LUA")
    for key, value in pairs(files) do
        AddCSLuaFile(pathFolder..value)
        include(pathFolder..value)
    end
    for key, value in pairs(directories) do
        LoadDirectory(pathFolder..value)
    end
end

/*
* Allows to check if the player is super admin or admin or in solo game.
* @Player ply The player whose permissions are being checked.
*/
function toucanlib.CheckPermissionPlayer(ply)
    if (!IsValid(ply)) then return false end
    if (!ply:IsPlayer()) then return false end
    if (ply:IsSuperAdmin() or ply:IsAdmin() or game.SinglePlayer()) then
        return true
    end
    return false
end

/*
* Allows to return the data of a file.
* @string path File path.
*/
function toucanlib.GetDataFromFile(path)
    local fileFind = file.Read(path) or ""
    local dataFind = util.JSONToTable(fileFind) or {}
    return dataFind
end

/*
* Generates a string containing a number of spaces equal to the argument passed in the method.
* @number number Number of space to increment.
*/
function toucanlib.AddSpaces(number)
    local spaces = ""
    for i = 1, number do
        spaces = spaces.." "
    end
    return spaces
end

-- [[ CLIENT ]]
if (CLIENT) then
    /*
    * Allows to return the size in pixel of a string.
    * @string text Text used to know its size in pixel.
    */
    function toucanlib.GetSizePixelText(text)
        surface.SetFont( "DermaDefault" )
		local witdh, height = surface.GetTextSize( text )
        return witdh, height
    end

    /*
    * Allows you to modify the data of a file on the server side.
    * @string path File path.
    * @any value value to assign.
    * @string key parameter to modify.
    */
    function toucanlib.SetDataFromClientToServer(path, value, key, keyCache)
        local dataToSend = {}
        dataToSend.data = value -- We do that because it can be a bool or a number.
        net.Start(TOUCAN_LIB_CONFIG.SetDataClientToServer)
            net.WriteString(path)
            net.WriteString(key)
            net.WriteTable(dataToSend)
            net.WriteString(keyCache)
        net.SendToServer()
    end
    
    net.Receive(TOUCAN_LIB_CONFIG.SynchronizeDataClient, function ( )
        local data = net.ReadTable()
        local key = net.ReadString()
        AEGIS_CONFIG[key] = data
    end)

    net.Receive(TOUCAN_LIB_CONFIG.SoundToPlayClientSide, function ( )
        local pathSound = net.ReadString()
        local isLoop = net.ReadBool()
        local ply = LocalPlayer()
        if (isLoop) then
            ply:StartLoopingSound(pathSound)
        else
            ply:EmitSound(pathSound)
        end
    end)
end

-- [[ SERVER ]]
if (SERVER) then
    /*
    * Allows you to synchronize server-side data to a player's client.
    * @string path File route.
    * @string key.
    * @Player ply The player data to synchronize to the server side data.
    */
    function toucanlib.SynchronizeDataClient(key, ply)
        local data = AEGIS_CONFIG[key]
        net.Start(TOUCAN_LIB_CONFIG.SynchronizeDataClient)
            net.WriteTable(data)
            net.WriteString(key)
        net.Send(ply)
    end

    /*
    * Allows you to synchronize server-side data to the client of all players.
    * @string path File route.
    */
    function toucanlib.SynchronizeDataClientAllPlayer(key)
        local data = AEGIS_CONFIG[key]
        net.Start(TOUCAN_LIB_CONFIG.SynchronizeDataClient)
            net.WriteTable(data)
            net.WriteString(key)
        net.Broadcast() 
    end

    /*
    * Allows to take a sound only on the client side of a player, so only he will hear it.
    * @Player ply The player who must hear the sound.
    * @string path File route.
    */
    function toucanlib.SoundToPlayClientSide(ply, soundToPlay, isLoop)
        net.Start(TOUCAN_LIB_CONFIG.SoundToPlayClientSide)
            net.WriteString(soundToPlay)
            net.WriteBool(isLoop)
        net.Send(ply)
    end

    -- Receive client-side data to be modified on the server side, then synchronize the client data of all players.
    net.Receive(TOUCAN_LIB_CONFIG.SetDataClientToServer, function (len, ply)
        if (toucanlib.CheckPermissionPlayer(ply)) then
            local pathFile = net.ReadString()
            local keyFile = net.ReadString()
            local valueFile = net.ReadTable()
            local keyCache = net.ReadString()
            local data = toucanlib.GetDataFromFile(pathFile)
            data[keyFile] = valueFile.data
            AEGIS_CONFIG[keyCache] = data
            file.Write(pathFile, util.TableToJSON(data, true))

            -- We synchronize all the players' data.
            toucanlib.SynchronizeDataClientAllPlayer(keyCache)
        else
            ply:ChatPrint("Are we cool yet ?") -- Sus
        end
    end)
end