# ToucanLib
Small library used on [Garry's Mod](https://store.steampowered.com/app/4000/Garrys_Mod/?l=french) by my addons when I use common features.<br>
However you can use it as you like if you are interested.

### 🛠️ Functionality
A documentation of all the existing features is available on this [link](https://gitlab.com/toucan-lab/toucanlib/-/wikis/home).

### ⚙️ Installations
Download the addon [here](https://www.flaticon.com/fr/icone-gratuite/toucan_1862325) and add it to your collection.
<br>
Or download the folder on this repo and drag it to your ```addons``` folder.

### 🧩 GameModes
Is compatible on all gamemodes normally, but I recommend its use on ```darkrp``` if possible.

### 📌 Dependencies

- None